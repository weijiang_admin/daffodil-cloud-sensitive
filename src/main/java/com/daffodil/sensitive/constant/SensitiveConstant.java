package com.daffodil.sensitive.constant;

/**
 * 
 * @author yweijian
 * @date 2022年9月19日
 * @version 2.0.0
 * @description
 */
public class SensitiveConstant {

    /**
     * api上下文地址
     */
    public static final String API_CONTENT_PATH = "/api-sensitive";
    
    /** 不文明词语 */
    public static final String SENSITIVE_WORD = "system:sensitive:word";
}
